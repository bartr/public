path "secret/user/*" {
	capabilities = ["read", "create", "update", "delete"]
}

path "secret/listing/*" {
	capabilities = ["read", "create", "update", "delete"]
}

path "secret/ext/pay/braintree/*" {
	capabilities = ["read"]
}

path "secret/ext/pay/stripe/*" {
	capabilities = ["read"]
}

path "secret/ext/geo/google" {
	capabilities = ["read"]
}
