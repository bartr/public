This directory should contain secrets to be populated into Vault
at server image time. The structure of secrets should match their expected
destination, i.e. the file `ext/geo/google.json` would create a secret in Vault
at `ext/geo/google`.

Expected secrets:

- `ext/geo/google` (api_key)
- `ext/pay/stripe/sandbox` (client_id, publishable, secret)
- `ext/pay/braintree/sandbox` (marketplace_master_merchant_account_id, merchant_id, private_key, public_key) (unused for now I think)
