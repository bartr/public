backend "consul" {
	address = "localhost:8500"
}

listener "tcp" {
	address = "localhost:8200"
	tls_disable = 1
}
