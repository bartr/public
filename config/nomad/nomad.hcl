bind_addr = "0.0.0.0"
data_dir = "/var/lib/nomad"

advertise {
	http = "{{GetInterfaceIP `eth0`}}"
	rpc = "{{GetInterfaceIP `eth0`}}"
	serf = "{{GetInterfaceIP `eth0`}}"
}

server {
	enabled = true
	bootstrap_expect = 1
}

client {
	enabled = true
	servers = ["{{GetInterfaceIP `eth0`}}"]
	options {
		"driver.raw_exec.enable" = "1"
	}
}

vault {
	enabled = true
	address = "http://localhost:8200"
}
