call plug#begin()
Plug 'tpope/vim-sensible'
Plug 'benmills/vimux'
Plug 'fatih/vim-go'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'jlanzarotta/bufexplorer'
Plug 'thomd/vim-wasabi-colorscheme'
call plug#end()

set hidden
set nu
nmap <Leader>ff :FZF<cr>

au filetype go nmap <Leader>tt :GoTest!<cr>
au filetype go nmap <Leader>tf :GoTestFunc!<cr>
au filetype go silent GoBuildTags js mage

silent! colorscheme wasabi256
