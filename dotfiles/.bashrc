export GOPATH=~/go
export GOBIN=${GOPATH}/bin
export PATH=${PATH}:${GOBIN}

BOLD=$(tput bold)
NORMAL=$(tput sgr0)

WORKSPACE=~/workspace/bartr
if [[ ! -d "${WORKSPACE}" ]]; then
	echo ''; echo "${BOLD}First time setup:${NORMAL} cloning repository..."
	mkdir -p $(dirname "${WORKSPACE}")
	git clone git@bitbucket.org:bartr/bartr.git "${WORKSPACE}"
fi
cd "${WORKSPACE}"
# As long as we're developing in GOPATH, we should vendor defined dependencies.
# GO111MODULE=on go mod vendor

if [[ ! -d "${GOBIN}" ]]; then
	echo ''; echo "${BOLD}First time setup:${NORMAL} installing command-line tools..."
	go get -u github.com/cespare/reflex \
		  github.com/ddollar/forego \
		  github.com/dradtke/stubber \
		  github.com/dradtke/gopherpc/cmd/gopherpc \
		  github.com/magefile/mage \
		  github.com/gopherjs/gopherjs

	echo "Running 'go generate'..."
	go generate
fi

if [[ ! -d "${HOME}/.vim/plugged" ]]; then
        echo ''; echo "${BOLD}First time setup:${NORMAL} installing Vim plugins..."
        vim +PlugInstall +qall
        vim +GoInstallBinaries +qall
fi


if [[ -z ${TMUX+x} ]]; then
        tmux attach || tmux
fi

export FZF_DEFAULT_COMMAND='ag -l -g ""'
